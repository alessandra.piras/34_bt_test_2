
## Name
34 BIG THINGS 3D RIGGER/TECH ANIMATOR TEST

## Description
This project contains two complete exercises for the application test: the 3D Rigger/ Tech Animator position in 34 Big Things videogame studio.

## External tools used
As specified in the file guides, you will need an add-on to see the rig UI correctly:
https://fin.gumroad.com/l/STdb
The rigs can be perfectly operated without it, but having a UI is always handful. 

## Documentation:
I used mainly the Rigging pipeline documentation provided by the studio to work on this project, with some integration from:
https://developer.blender.org/T71068 for solving an error derived from bones inheriting scales uncorrectly after baking the animation inside blender and unreal engine. 


## Authors and acknowledgment
Alessandra Piras

## Project status
Finished
